#include<stdio.h>
void input(int *a, int *b)
{
  printf("Enter first number: ");
  scanf("%d",&*a);
  printf("Enter second number: ");
  scanf("%d",&*b);
}
void swap(int *a, int*b)
{
  int temp;
  temp=*b;
  *b=*a;
  *a=temp;
}
void output(int a, int b)
{
  printf("After Swapping: ");
  printf("\nFirst number: ");
  printf("%d",a);
  printf("\nSecond number: ");
  printf("%d\n",b);
}
int main()
{
  int a,b;
  input(&a,&b);
  swap(&a,&b);
  output(a,b);
  return 0;
}