#include<stdio.h>
void input(int n, int *A)
{
   int i;
   printf("Enter the array elements: ");
   for(i=0;i<n;i++)
    scanf("%d",&A[i]); 
}
void interchange(int n,int *A)
{
  int i,small=10000,big=0,s,b,temp;
  for(i=0;i<n;i++)
  {
   if(small>A[i])
   {
    small=A[i];
    s=i;
   }
   if(big<A[i])
   {
     big=A[i];
     b=i;
   }
  }
  temp=A[s];
  A[s]=A[b];
  A[b]=temp;
}
void output(int *A,int n)
{
   int i;
    printf("The updated array elements are: ");
   for(i=0;i<n;i++)
       printf("%d ",A[i]);  
}
int main()
{
  int n;
  printf("Enter the no. of values to be entered in the array: ");
  scanf("%d",&n);
  int A[n];
  input(n,A);
  interchange(n,A);
  output(A,n);
  return 0;
} 