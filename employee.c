#include<stdio.h>
struct Employee
{
  int EId;
  char EName[20];
  long int Sal;
  char DOJ[10];
};
void input(struct Employee *E)
{
  printf("Enter Employee ID: ");
  scanf("%d",&E->EId);
  printf("Enter Employee Name: ");
  scanf("%s",E->EName);
  printf("Enter Salary: ");
  scanf("%ld",&E->Sal);
  printf("Enter Date of Joining(DD-MM-YYYY): ");
  scanf("%s",E->DOJ);
}
void output(struct Employee *E)
{
  printf("\nEmployee ID: ");
  printf("%d",E->EId);
  printf("\nEmployee Name: ");
  printf("%s",E->EName);
  printf("\nSalary: ");
  printf("%ld",E->Sal);
  printf("\nDate of Joining: ");
  printf("%s\n",E->DOJ);
}
int main()
{
  struct Employee E;
  input(&E);
  output(&E);
  return 0;
}