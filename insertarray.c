#include<stdio.h>
void input(int n, int *A)
{
   int i;
   printf("Enter the array elements: ");
   for(i=0;i<n;i++)
    scanf("%d",&A[i]); 
}
void insert(int *n,int *A,int pos,int val)
{
  int i;
  for(i=*n-1;i>=(pos-1);i--)
    A[i+1]=A[i];
  A[pos-1]=val;
  (*n)++;
}
void output(int *A,int n)
{
   int i;
    printf("The updated array elements are: ");
   for(i=0;i<n;i++)
       printf("%d ",A[i]);  
}
int main()
{
  int n,pos,val;;
  printf("Enter the no. of values to be entered in the array: ");
  scanf("%d",&n);
  int A[n+100];
  input(n,A);
  printf("Enter the position and value of the number to be inserted: ");
  scanf("%d%d",&pos,&val);
  insert(&n,A,pos,val);
  output(A,n);
  return 0;
}   