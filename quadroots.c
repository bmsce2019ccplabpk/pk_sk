#include<stdio.h>
#include<math.h>
void Calcroots(int,int,int);
int main()
{
  float a,b,c;
  printf("Enter the coefficient of x^2 term\n");
  scanf("%f",&a);
  printf("Enter the coefficient of x term\n");
  scanf("%f",&b);
  printf("Enter the constant term\n");
  scanf("%f",&c);
  Calcroots(a,b,c);
  return 0;
}
void Calcroots(int a,int b,int c)
{
    float D,r,i,r1,r2;
    D=(b*b)-(4*a*c);
  if(D<0)
  {
      r=-b/(2.0*a);
      i=sqrt(-D)/(2.0*a);
      printf("The roots of the Quadratic equation are imaginary\n");
      printf("The roots of the Quadratic equation are:\n");
      printf("%f + %fi\n",r,i);
    printf("%f - %fi\n",r,i);
  }
  else
  {
    r1= (-b+sqrt(D))/(2.0*a);
    r2= (-b-sqrt(D))/(2.0*a);
    printf("The roots of the Quadratic Equation are %f and %f\n",r1,r2);
  }
}  
  