#include<stdio.h>
int input();
int check(int);
void output(int);
int main()
{
  int num,flag;
  num=input();
  flag=check(num);
  output(flag);
  return 0;
}
int input()
{
   int num;
  printf("Enter a number ");
  scanf("%d",&num);
  return num; 
}
int check(int num)
{
    int i,flag;
    if(num==1)
      flag=1;
  else
  {
    for(i=2;i<=num;i++)
    {
    if(num%i==0)
      if(i!=num)
      {
        flag=2;
        break;
      }
      else
        flag=0;
    }
  }
  return flag; 
}
void output(int flag)
{
    if(flag==1)
        printf("The number is neither prime nor composite\n");
    else if(flag==0)
        printf("The number is prime\n");
    else
        printf("The number is composite\n");
}