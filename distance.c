#include<stdio.h>
#include<math.h>
void input(float *,float *,float *,float *);
float distance(float,float,float,float);
void output(float);
int main()
{
  float dist,x1,y1,x2,y2;
  input(&x1,&y1,&x2,&y2);
  dist=distance(x1,y1,x2,y2);
  output(dist);
  return 0;
}
float distance(float x1,float y1,float x2,float y2)
{
  float dist;
  dist = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
  return dist;
}
void input(float *x1,float *y1,float *x2,float *y2)
{
  printf("Enter the coordinates of the first point(x followed by y)\n");
  scanf("%f%f",&*x1,&*y1);
  printf("Enter the coordinates of the second point(x followed by y)\n");
  scanf("%f%f",&*x2,&*y2);
}
void output(float d)
{
   printf("Distance = %f\n",d); 
}