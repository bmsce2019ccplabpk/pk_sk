#include <stdio.h>
int main()
{
    int x,y;
    int sum,diff,multi,rem;
    float div;
    printf("Enter two integers\n");
    scanf("%d %d",&x,&y);
    sum=x+y;
    diff=x-y;
    multi=x*y;
    div= (float)x/y;
    rem=x%y;
    printf("Sum = %d\n",sum);
    printf("Difference = %d\n",diff);
    printf("Product = %d\n",multi);
    printf("Division = %f\n",div);
    printf("Reminder = %d\n",rem);
    return 0;
}