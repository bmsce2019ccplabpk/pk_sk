#include<stdio.h>
void input(int A[5][3])
{
  int i,j;
  for(i=0;i<5;i++)
  {
    printf("Enter the marks of student %d in",i+1);
    for(j=0;j<3;j++)
    {
      printf("\nSubject %d: ",j+1);
      scanf("%d",&A[i][j]);
    }
    printf("\n");
  }
}
void highest(int A[5][3], int *subj1, int *subj2, int *subj3)
{
  int i;
  for(i=0;i<5;i++)
  {
    if(*subj1<A[i][0])
      *subj1=A[i][0];
    if(*subj2<A[i][1])
      *subj2=A[i][1];
    if(*subj3<A[i][2])
      *subj3=A[i][2];
  }
}
void output(int h1,int h2,int h3)
{
  printf("Highest marks in Subject 1 is %d\n",h1);
  printf("Highest marks in Subject 2 is %d\n",h2);
  printf("Highest marks in Subject 3 is %d\n",h3);
}
int main()
{
  int A[5][3],subj1=0,subj2=0,subj3=0;
  input(A);
  highest(A,&subj1,&subj2,&subj3);
  output(subj1,subj2,subj3);
  return 0;
}