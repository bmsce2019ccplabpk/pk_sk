#include<stdio.h>
#include<stdlib.h>
void input(FILE *f)
{
  char ch;
  f=fopen("INPUT.TXT","w");
  if(f==NULL)
  {
    printf("File Not Created !! Error!!");
    exit(0);
  }
  else
  {
    printf("Enter text to write in the file: ");
    while((ch=getchar())!='\n')
    {
      putc(ch,f);
    }
    fclose(f);
  }
}
void output(FILE *f)
{
  char ch;
  f=fopen("INPUT.TXT","r");
  if(f==NULL)
  {
    printf("File cannot be opened!! Error!!");
    exit(0);
  }
  else
  {
    printf("Contents of the file is: ");
    while((ch=getc(f))!=EOF)
    {
      printf("%c",ch);

    }
    printf("\n");
    fclose(f);
  } 
}
int main()
{
  FILE *f;
  input(f);
  output(f);
  return 0;
}