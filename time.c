#include<stdio.h>
void Input(int *,int *);
int Convert(int,int);
void Output(int);
int main()
{
  int hr,min,time;
  Input(&hr,&min);
  time=Convert(hr,min);
  Output(time);
  return 0;
}
void Input(int *x, int *y)
{
  printf("Enter the time in hours and minutes\n");
  scanf("%d%d",&*x,&*y);
}
int Convert(int hr, int min)
{
  return ((60*hr)+min);
}
void Output(int ans)
{
  printf("The time in minutes is %d ",ans);
}
  
  