#include<stdio.h>
int count=0,sum=0;
float input();
float Average(int);
void output(float);
int main()
{
  float avg;
  avg=input();
  output(avg);
  return 0;
}
float input()
{
  int num,i,n;
  float avg;
  printf("Enter the no. of values to be entered ");
  scanf("%d",&n);
  for(i=1;i<=n;i++)
  {
    printf("Enter number #%d ",i);
    scanf("%d",&num);
    avg=Average(num);
   }
   return avg;
}
float Average(int num)
{
    float avg;
    if(num>0)
    {
      count++;
      sum+=num;
    }
    avg= (float)sum/count;
    return avg;
}
void output(float avg)
{
    printf("Average of positive numbers is %f",avg);
}