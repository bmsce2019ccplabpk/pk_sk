#include<stdio.h>
void input(char *A, char *B)
{
  printf("Enter first string: ");
  scanf(" %s",A);
  printf("Enter second string: ");
  scanf(" %s",B);
}
void concatenate(char *A, char *B, char *C)
{
  int i=0,j=0;
  while(A[i]!='\0')
  {
    C[j]=A[i];
    i++;
    j++;
  }
  i=0;
  while(B[i]!='\0')
  {
    C[j]=B[i];
    i++;
    j++;
  }
  C[j]='\0';
}
void output(char *C)
{
  printf("The concatenated string is: ");
  printf(" %s\n",C);
}
int main()
{
  char A[15],B[15],C[30];
  input(A,B);
  concatenate(A,B,C);
  output(C);
  return 0;
}