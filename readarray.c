#include<stdio.h>
void input(int n, int *A)
{
   int i;
   printf("Enter the array elements: ");
   for(i=0;i<n;i++)
      scanf("%d",&A[i]); 
}
void output(int *A,int n)
{
   int i;
   printf("The array elements are: ");
   for(i=0;i<n;i++)
   {
      printf("%d ",A[i]);  
   }

}
int main()
{
   int n;
   printf("Enter the no. of values to be entered in the array: ");
   scanf("%d",&n);
   int A[n];
   input(n,&A);
   output(A,n);
   return 0;
}