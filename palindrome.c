#include <stdio.h>
int input();
int reverse(int);
void output(int);
int main()
{
    int num,rev,check;
    num=input();
    rev=reverse(num);
    check=palindrome(rev,num);
    output(check);
    return 0;
}
int input()
{
    int num;
    printf("Enter a number ");
    scanf("%d",&num);
    return num; 
}
int reverse(int num)
{
    int dig,rev=0;
    while(num!=0)
    {
      dig=num%10;
      rev=(rev*10)+dig;
      num=num/10;
    }
    return rev; 
}
int palindrome(int rev,int num)
{
    if(rev==num)
        return 1;
    else 
        return 0;
}
void output(int check)
{
    if(check==1)
      printf("The number is a Palindrome\n"); 
    else
      printf("The number is not a Palindrome\n");
}