#include<stdio.h>
void input(int *,int *,int *);
int check(int,int,int);
void output(int);
int main()
{
  int largest,a,b,c;
  input(&a,&b,&c);
  largest=check(a,b,c);
  output(largest);
  return 0;
}
void input(int *a,int *b,int *c)
{
   printf("Enter three integer numbers\n");
   scanf("%d%d%d",&*a,&*b,&*c);
}
int check(int a,int b,int c)
{
    int check;
    check = a>=b?(a>=c?a:c):(b>=c?b:c) ;
    return check;
}
void output(int large)
{
    printf("Largest number is %d\n",large);
}