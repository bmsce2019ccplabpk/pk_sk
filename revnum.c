#include <stdio.h>
int input();
int reverse(int);
void output(int);
int main()
{
    int num,rev;
    num=input();
    rev=reverse(num);
    output(rev);
    return 0;
}
int input()
{
    int num;
    printf("Enter a number ");
    scanf("%d",&num);
    return num; 
}
int reverse(int num)
{
    int dig,rev=0;
    while(num!=0)
    {
      dig=num%10;
      rev=(rev*10)+dig;
      num=num/10;
    }
    return rev; 
}
void output(int rev)
{
    printf("The reverse of the number is %d",rev); 
