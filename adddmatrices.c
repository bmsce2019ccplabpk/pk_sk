#include<stdio.h>
void input(int A[][20], int B[][20], int m, int n)
{
    int i,j;
    printf("Enter the elements of the first matrix ");
    for(i=0;i<m;i++)
        for(j=0;j<n;j++)
            scanf("%d",&A[i][j]);
    printf("Enter the elements of the second matrix ");
    for(i=0;i<m;i++)
        for(j=0;j<n;j++)
            scanf("%d",&B[i][j]);
}
void add(int A[][20], int B[][20], int C[][20], int m, int n)
{
    int i,j;
    for(i=0;i<m;i++)
        for(j=0;j<n;j++)
            C[i][j]=A[i][j]+B[i][j];
}
void output(int C[][20], int m, int n)
{
    int i,j;
    printf("The sum of the matrices is:\n");
    for(i=0;i<m;i++)
    {
      for(j=0;j<n;j++)
        printf("%d  ",C[i][j]);
      printf("\n");
    }
}
int main()
{
    int m,n;
    int A[20][20],B[20][20],C[20][20];
    printf("Enter the no. of rows and columns of the matices to be entered");
    scanf("%d%d",&m,&n);
    input(A,B,m,n);
    add(A,B,C,m,n);
    output(C,m,n);
    return 0;
}